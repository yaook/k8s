variables:
  YAOOK_K8S_RELEASE_BRANCH_PREFIX: release/v
  YAOOK_K8S_RELEASE_PREPARE_BRANCH_PREFIX: release-prepare/v
  YAOOK_K8S_RELEASE_HOTFIX_BRANCH_PREFIX: hotfix/
  YAOOK_K8S_DEFAULT_BRANCH: devel

  REGEX_RELEASE_BRANCH_PREFIX: /^release\/v\S+$/
  REGEX_RELEASE_PREPARE_BRANCH_PREFIX: /^release-prepare\/v\S+$/
  REGEX_RELEASE_HOTFIX_BRANCH_PREFIX: /^hotfix\/\S+$/
  REGEX_RELEASE_HOTFIX_BASE_BRANCH_PREFIX: /^hotfix\/base\S+$/

  YAOOK_K8S_CI_IMAGE_NAME: ${CI_REGISTRY_IMAGE}/ci
  YAOOK_K8S_CI_IMAGE_TAG:
    description: "The container image tag that should be used for this pipeline"
  SKIP_HOUSEKEEPING:
    description: "Whether the housekeeping stage should be skipped in this pipeline"
    value: "false"
    options:
    - "false"
    - "true"
  SKIP_LINT:
    description: "Whether the lint stage should be skipped in this pipeline"
    value: "false"
    options:
    - "false"
    - "true"

# Pipelines run on devel branch, release-(prepare)-branches and merge requests
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $YAOOK_K8S_DEFAULT_BRANCH
    - if: '$CI_COMMIT_BRANCH =~ $REGEX_RELEASE_PREPARE_BRANCH_PREFIX'
    - if: '$CI_COMMIT_BRANCH =~ $REGEX_RELEASE_BRANCH_PREFIX'
    - if: $CI_MERGE_REQUEST_EVENT_TYPE
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_PIPELINE_SOURCE == "web"

stages:
- build-image  # needs to run first (see !1627)
- housekeeping
- lint
- hotfix-prepare
- cluster-tests
- diagnostic-tools
- tag-image
- release

include:
  - local: ci/build-image.yaml
  - local: ci/hotfix-prepare.yaml
  - local: ci/cluster-tests.yaml
  - local: ci/diagnostic-tools.yaml
  - local: ci/release.yaml


# always lint on merge trains
.default_lint_rules:
  rules:
    - if: $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"
      when: on_success

tflint:
  image:
    name: "ghcr.io/terraform-linters/tflint:v0.55.1"
    entrypoint: ["/bin/sh", "-c"]
  stage: lint
  script:
    - tflint --chdir=terraform/
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - 'terraform/**/*'
      if: $SKIP_LINT != "true"
      when: on_success
    - when: never

shellcheck:
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/koalaman/shellcheck-alpine:v0.10.0"
  stage: lint
  script:
    - "find -iname '*.sh' '!' -ipath './.git/**' -print0 | xargs -0 -- shellcheck -Calways"
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - '**/*.sh'
      if: $SKIP_LINT != "true"
      when: on_success
    - when: never

yamllint:
  stage: lint
  tags:
    - docker
  image: registry.gitlab.com/pipeline-components/yamllint:latest
  script:
    - yamllint .
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - '**/*.{yaml,yml}'
      if: $SKIP_LINT != "true"
      when: on_success
    - when: never

detect-vault-image:
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  stage: lint
  script:
    - bash ./actions/detect-vault-image.sh
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - when: on_success
      if: $SKIP_LINT != "true"

ansible-lint:
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  stage: lint
  script:
    - "ansible-galaxy install -r ansible/requirements.yaml"
    - "ANSIBLE_ROLES_PATH=./k8s-core/ansible/roles:./k8s-supplements/ansible/roles:./k8s-supplements/ansible/test-roles ansible-lint -c ci/lint/ansible-lint-conf --offline"
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - when: on_success
      if: $SKIP_LINT != "true"

flake8:
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  script:
    - 'python3 -m flake8'
  stage: lint
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - '**/*.py'
      if: $SKIP_LINT != "true"
      when: on_success
    - when: never

render-options:
  stage: housekeeping
  needs:
    - job: build_image # otherwise it would be run at the same time as build_image (and then fail) if the optional job was skipped
      optional: true
    - job: release-note-file-check
      optional: true
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  before_script:
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
  script:
    - bash ./ci/housekeeping/render-options.sh
  artifacts:
    paths:
      - public/
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - nix/yk8s/**/*
      if: $SKIP_HOUSEKEEPING != "true"
      when: on_success
    - when: never

pre-commit-hooks:
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  script:
    - 'SKIP=shellcheck,yamllint,flake8,check-flake pre-commit run --all-files || true'
    - git --no-pager diff
    - git restore .
    - 'SKIP=shellcheck,yamllint,flake8,check-flake pre-commit run --all-files'
  stage: lint
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - when: on_success
      if: $SKIP_LINT != "true"

build-docs-check:
  stage: lint
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  script:
    - towncrier build --version x.x.x --keep
    - sphinx-build -W docs _build/html
    - mv _build/html public
  artifacts:
    expose_as: "Rendered Docs"
    expire_in: "7 days"
    paths:
      - public/
  rules:
    # run only on MR when changes were made
    - if: '($CI_MERGE_REQUEST_EVENT_TYPE == "detached" || $CI_MERGE_REQUEST_EVENT_TYPE == "merged_result") && ($SKIP_LINT != "true")'
      changes:
      - 'docs/**/*'
      - 'docs/*'  # the above does not include files directly in docs/
      - 'CHANGELOG.rst'
      when: on_success
    - when: never
  tags:
    - docker

check-key-expiry:
  stage: lint
  image: "nixery.dev/shell/coreutils/findutils/gnupg/gnugrep/file"
  script:
  - ci/lint/check-key-expiry.sh

  rules:
    - !reference [.default_lint_rules, rules]
    # run only on MR when changes were made
    - if: '($CI_MERGE_REQUEST_EVENT_TYPE == "detached" || $CI_MERGE_REQUEST_EVENT_TYPE == "merged_result") && ($SKIP_LINT != "true")'
      changes:
      - '**/*.gpg'
      when: on_success
    # always run on devel branch
    - if: $CI_COMMIT_REF_NAME == $YAOOK_K8S_DEFAULT_BRANCH
      when: on_success
    - when: never
  tags:
    - docker

release-note-file-check:
  stage: housekeeping
  image: "${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  variables:
    CONFIG_FILE: towncrier.toml
    HOTFIX: "False"
    SOURCE_BRANCH: origin/${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
    TARGET_BRANCH: origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
    COMMIT_MESSAGE: MR-number for releasenote(s) changed
    COMMIT_FILES: docs/_releasenotes
  before_script:
    - git fetch origin "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
    - |
      if [ "${CI_MERGE_REQUEST_PROJECT_URL}" = "${CI_MERGE_REQUEST_SOURCE_PROJECT_URL}" ]; then
        FORK=False
      else
        FORK=True
      fi
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    # we need to catch the exit-code from python as it otherwise will not be transmitted correctly
    - exit_code=0
    - python3 ci/housekeeping/check-releasenote-file.py "${CI_PROJECT_DIR}" "${TARGET_BRANCH}" "${CONFIG_FILE}" "${CI_MERGE_REQUEST_IID}" "${COMMIT_FILES}" "${FORK}" "${HOTFIX}" || exit_code=$?
    - echo $exit_code
    - towncrier build --version x.x.x --config "${CONFIG_FILE}" --draft
    - |
      if [ "${exit_code}" != 0 ]; then
        exit "${exit_code}"
      fi
    - |
      if [ "${FORK}" = "False" ]; then
        git fetch origin "${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
        git checkout "${SOURCE_BRANCH}"
        git status
        CHANGES=$(git diff "${SOURCE_BRANCH}" --name-only -- "${COMMIT_FILES}" | wc -l)
        if [ "${CHANGES}" -gt 0 ]; then
          echo "committing"
          git add ${COMMIT_FILES}
          git commit --amend --no-edit
          git push --force -o ci.skip "https://gitlab-ci-token:${PUSH_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}" HEAD:"${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
        fi
      fi
  tags:
    - docker
  retry: 1
  allow_failure:
    exit_codes: 13 # if number in hotfix doesn't match MR-IID, exit with warning
  rules:
    # don't run when CHANGELOG.rst has been edited
    - if: $SKIP_HOUSEKEEPING != "true"
      changes:
      - CHANGELOG.rst
      when: never
    # run only for MRs from hotfixing-branches ..
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $REGEX_RELEASE_HOTFIX_BRANCH_PREFIX'
      variables:
        COMMIT_FILES: docs/_releasenotes/hotfix
        HOTFIX: "True"
      when: on_success
    # .. and to devel
    - if:  $CI_MERGE_REQUEST_EVENT_TYPE == "merged_result" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $YAOOK_K8S_DEFAULT_BRANCH
      when: on_success
    - when: never

pages-dry-run:
  stage: lint
  image: !reference [.pages, image]
  before_script: !reference [.pages, before_script]
  script:
  - !reference [.pages, script]
  - mv _build/html private
  tags: !reference [.pages, tags]
  dependencies: !reference [.pages, dependencies]
  artifacts:
    expose_as: "Rendered Multiversion Docs"
    expire_in: "7 days"
    paths:
      - private/
  rules:
    - if: $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"
      when: on_success
    - changes:
      - 'docs/**/*'
      - 'docs/*'  # the above does not include files directly in docs/
      - 'CHANGELOG.rst'
      #  sphinx-multiversion may be updated
      - nix/dependencies.nix
      - flake.lock
      # sphinx-multiversion always uses this file from the current branch, so changes may break other versions
      - docs/conf.py
      if: $SKIP_LINT != "true"
      when: on_success
    - when: manual
      allow_failure: true
