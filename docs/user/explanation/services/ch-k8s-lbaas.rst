ch-k8s-lbaas
============

The ch-k8s-lbaas load-balancing solution resides in its own
`official repository <https://github.com/cloudandheat/ch-k8s-lbaas>`__.
Its configuraton reference can be found at :ref:`configuration-options.yk8s.ch-k8s-lbaas`

Functionality Visualization
---------------------------

.. figure:: /img/ch-k8s-lbaas-overview.svg
   :scale: 100%
   :alt: ch-k8s-lbaas Overview Visualization
   :align: center

|
