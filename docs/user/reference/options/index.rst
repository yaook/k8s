Configuration Options
#####################


.. toctree::
  :maxdepth: 2
  :hidden:

  yk8s.ch-k8s-lbaas
  yk8s.infra
  yk8s.ipsec
  yk8s.k8s-service-layer.cert-manager
  yk8s.k8s-service-layer.etcd-backup
  yk8s.k8s-service-layer.fluxcd
  yk8s.k8s-service-layer.ingress
  yk8s.k8s-service-layer.prometheus
  yk8s.k8s-service-layer.rook
  yk8s.k8s-service-layer.vault
  yk8s.kubernetes.kubelet
  yk8s.kubernetes.local_storage.dynamic
  yk8s.kubernetes.local_storage.static
  yk8s.kubernetes.network.calico
  yk8s.kubernetes.network
  yk8s.kubernetes
  yk8s.kubernetes.storage
  yk8s.load-balancing
  yk8s.miscellaneous
  yk8s.node-scheduling
  yk8s.nvidia
  yk8s.openstack
  yk8s.terraform
  yk8s.testing
  yk8s.vault
  yk8s.wireguard
  
:doc:`ch-k8s-lbaas <yk8s.ch-k8s-lbaas>`

:doc:`infra <yk8s.infra>`

:doc:`ipsec <yk8s.ipsec>`

:doc:`k8s-service-layer.cert-manager <yk8s.k8s-service-layer.cert-manager>`

:doc:`k8s-service-layer.etcd-backup <yk8s.k8s-service-layer.etcd-backup>`

:doc:`k8s-service-layer.fluxcd <yk8s.k8s-service-layer.fluxcd>`

:doc:`k8s-service-layer.ingress <yk8s.k8s-service-layer.ingress>`

:doc:`k8s-service-layer.prometheus <yk8s.k8s-service-layer.prometheus>`

:doc:`k8s-service-layer.rook <yk8s.k8s-service-layer.rook>`

:doc:`k8s-service-layer.vault <yk8s.k8s-service-layer.vault>`

:doc:`kubernetes.kubelet <yk8s.kubernetes.kubelet>`

:doc:`kubernetes.local_storage.dynamic <yk8s.kubernetes.local_storage.dynamic>`

:doc:`kubernetes.local_storage.static <yk8s.kubernetes.local_storage.static>`

:doc:`kubernetes.network.calico <yk8s.kubernetes.network.calico>`

:doc:`kubernetes.network <yk8s.kubernetes.network>`

:doc:`kubernetes <yk8s.kubernetes>`

:doc:`kubernetes.storage <yk8s.kubernetes.storage>`

:doc:`load-balancing <yk8s.load-balancing>`

:doc:`miscellaneous <yk8s.miscellaneous>`

:doc:`node-scheduling <yk8s.node-scheduling>`

:doc:`nvidia <yk8s.nvidia>`

:doc:`openstack <yk8s.openstack>`

:doc:`terraform <yk8s.terraform>`

:doc:`testing <yk8s.testing>`

:doc:`vault <yk8s.vault>`

:doc:`wireguard <yk8s.wireguard>`

