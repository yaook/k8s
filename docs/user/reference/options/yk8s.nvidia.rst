.. _configuration-options.yk8s.nvidia:

yk8s.nvidia
^^^^^^^^^^^



.. _configuration-options.yk8s.nvidia.vgpu.driver_blob_url:

``yk8s.nvidia.vgpu.driver_blob_url``
####################################

Should point to a object store or otherwise web server, where the vGPU Manager installation file is available.


**Type:**::

  non-empty string


**Declared by**
https://gitlab.com/yaook/k8s/-/tree/devel/nix/yk8s/nvidia.nix


.. _configuration-options.yk8s.nvidia.vgpu.manager_filename:

``yk8s.nvidia.vgpu.manager_filename``
#####################################

Should hold the name of the vGPU Manager installation file.


**Type:**::

  non-empty string


**Declared by**
https://gitlab.com/yaook/k8s/-/tree/devel/nix/yk8s/nvidia.nix

