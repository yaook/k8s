# User Tutorials

```{toctree}
---
maxdepth: 2
hidden: true
---

create-cluster
upgrade-release
```

User facing tutorials on YAOOK/K8s.

---

::::{grid} 2
:::{grid-item-card} Cluster Creation
:link: /user/tutorial/create-cluster
:link-type: doc
Tutorial on how to create a YAOOK/K8s cluster.
:::
:::{grid-item-card} Release upgrade
:link: /user/tutorial/upgrade-release
:link-type: doc
Tutorial on how to upgrade to a new YAOOK/K8s release.
:::
::::
