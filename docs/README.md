# YAOOK/K8s Handbook

This documentation is generated using sphinx.

## Table of Contents

See [index.md](index.md).

Build the documentation by running the below from the repository's root.

```shell
# Build documentation
nix build .#docs

# Open in Firefox
firefox result/index.html
```
