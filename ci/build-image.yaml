build_image:
  tags:
    - docker
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/nixos/nix:2.26.2"
  stage: build-image
  rules:
    - if: ($YAOOK_K8S_CI_IMAGE_TAG == null) || ($YAOOK_K8S_CI_IMAGE_TAG == "")
  before_script:
  - cp ci/container-image/nix.conf /etc/nix/nix.conf
  script:
  - export YAOOK_K8S_CI_IMAGE_TAG="$(nix eval .#ciImage --read-only --raw | grep -Po '^/nix/store/\K[a-z0-9]{32}')"
  - export IMAGE_REF="${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}"
  # Manually talking to the registry API, because downloading skopeo only for the lookup would cost ~1min per run
  # Also jq is not available in the Nixos container image
  - token="$(curl "https://gitlab.com/jwt/auth?scope=repository:yaook/k8s/ci:pull&service=container_registry" | tr -d '\n\t\r ' | grep -oP '"token"\s*:\s*"\K.*?(?=")')"
  - |
    registry_response="$( \
        curl --head --silent --output /dev/null --write-out '%{http_code}' \
            --header "Authorization: Bearer ${token}" \
            "https://${YAOOK_K8S_CI_IMAGE_NAME%%/*}/v2/${YAOOK_K8S_CI_IMAGE_NAME#*/}/manifests/${YAOOK_K8S_CI_IMAGE_TAG}" \
    )"

    if [ "${registry_response}" -eq 200 ]; then
      echo "Image \"${IMAGE_REF}\" already present in the registry. Skipping build."
    else
      echo "Image \"${IMAGE_REF}\" not yet present in the registry. Building..."
      nix run nixpkgs#skopeo -- login -u gitlab-ci-token -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
      nix run ".#streamCiImage" | nix run nixpkgs#gzip -- --fast | nix run nixpkgs#skopeo -- --insecure-policy copy docker-archive:/dev/stdin "docker://${IMAGE_REF}"
    fi
  - echo "YAOOK_K8S_CI_IMAGE_TAG=$YAOOK_K8S_CI_IMAGE_TAG" | tee image.env
  artifacts:
    reports:
      dotenv: image.env


.deploy_image:
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/nixos/nix:2.26.2"
  stage: tag-image
  before_script:
    - |
      cat > /etc/nix/nix.conf <<EOF
      build-users-group = nixbld
      sandbox = false
      extra-experimental-features = nix-command flakes
      EOF

deploy_image_default_branch:
  image: !reference [.deploy_image, image]
  stage: !reference [.deploy_image, stage]
  before_script: !reference [.deploy_image, before_script]
  rules:
    - if: $CI_COMMIT_REF_NAME == $YAOOK_K8S_DEFAULT_BRANCH
      when: on_success
  script:
  - nix run nixpkgs#skopeo -- login -u gitlab-ci-token -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
  - nix run nixpkgs#skopeo -- --insecure-policy copy "docker://${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}" "docker://${YAOOK_K8S_CI_IMAGE_NAME}:latest";
  tags:
  - docker

deploy_image_release:
  image: !reference [.deploy_image, image]
  stage: !reference [.deploy_image, stage]
  before_script: !reference [.deploy_image, before_script]
  rules:
    - if: '$CI_COMMIT_BRANCH =~ $REGEX_RELEASE_BRANCH_PREFIX'
      when: on_success
  script:
  - nix run nixpkgs#skopeo -- login -u gitlab-ci-token -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
  - nix run nixpkgs#skopeo -- --insecure-policy copy "docker://${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}" "docker://${YAOOK_K8S_CI_IMAGE_NAME}:$(cat ./version)"
  - nix run nixpkgs#skopeo -- --insecure-policy copy "docker://${YAOOK_K8S_CI_IMAGE_NAME}:${YAOOK_K8S_CI_IMAGE_TAG}" "docker://${YAOOK_K8S_CI_IMAGE_NAME}:$(cat ./version | cut -d'.' -f"1 2")"
  tags:
  - docker
