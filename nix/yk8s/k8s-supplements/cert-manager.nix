{
  config,
  lib,
  yk8s-lib,
  ...
}: let
  cfg = config.yk8s.k8s-service-layer.cert-manager;
  inherit (lib) mkEnableOption mkOption types;
  inherit (yk8s-lib) mkTopSection mkGroupVarsFile;
in {
  options.yk8s.k8s-service-layer.cert-manager = mkTopSection {
    _docs.preface = ''
      The used Cert-Manager controller setup will be explained in more detail
      soon :)

        .. note::

            To enable cert-manager,
            ``k8s-service-layer.cert-manager.enabled`` needs to be set to
            ``true``.
    '';

    enabled = mkEnableOption "management of a cert-manager.io instance";
    namespace = mkOption {
      description = ''
        Configure in which namespace the cert-manager is run. The namespace is
        created automatically, but never deleted automatically.
      '';
      type = types.nonEmptyStr;
      default = "k8s-svc-cert-manager";
    };
    install = mkOption {
      description = ''
        Install or uninstall cert manager. If set to false, the cert-manager will be
        uninstalled WITHOUT CHECK FOR DISRUPTION!
      '';
      type = types.bool;
      default = true;
    };
    helm_repo_url = mkOption {
      type = types.nonEmptyStr;
      default = "https://charts.jetstack.io";
    };
    chart_ref = mkOption {
      type = types.nonEmptyStr;
      default = "jetstack/cert-manager";
    };
    chart_version = mkOption {
      type = types.nonEmptyStr;
      # renovate: datasource=helm depName=cert-manager registryUrl=https://charts.jetstack.io
      default = "1.17.1";
    };
    release_name = mkOption {
      type = types.nonEmptyStr;
      default = "cert-manager";
    };
    scheduling_key = mkOption {
      description = ''
        Scheduling key for the cert manager instance and its resources. Has no
        default.
      '';
      type = with types; nullOr nonEmptyStr;
      default = null;
    };
    letsencrypt_email = mkOption {
      description = ''
        If given, a *cluster wide* Let's Encrypt issuer with that email address will
        be generated. Requires an ingress to work correctly.
        DO NOT ENABLE THIS IN CUSTOMER CLUSTERS, BECAUSE THEY SHOULD NOT CREATE
        CERTIFICATES UNDER OUR NAME. Customers are supposed to deploy their own
        ACME/Let's Encrypt issuer.
      '';
      type = with types; nullOr nonEmptyStr;
      default = null;
    };
    letsencrypt_preferred_chain = mkOption {
      description = ''
        By default, the ACME issuer will let the server choose the certificate chain
        to use for the certificate. This can be used to override it.
      '';
      type = with types; nullOr nonEmptyStr;
      default = null;
    };
    letsencrypt_ingress = mkOption {
      description = ''
        The ingress class to use for responding to the ACME challenge.
        The default value works for the default k8s-service-layer.ingress
        configuration and may need to be adapted in case a different ingress is to be
        used.
      '';
      type = types.nonEmptyStr;
      default = "nginx"; # TODO: get value from config.yk8s.k8s-service-layer.ingress once we have extraValues
    };
    letsencrypt_server = mkOption {
      description = ''
        This variable let's you specify the endpoint of the ACME issuer. A common usecase
        is to switch between staging and production.
        See https://letsencrypt.org/docs/staging-environment/
      '';
      type = types.nonEmptyStr;
      default = "https://acme-v02.api.letsencrypt.org/directory";
      example = "https://acme-staging-v02.api.letsencrypt.org/directory";
    };
  };
  config.yk8s._inventory_packages = [
    (
      mkGroupVarsFile {
        inherit cfg;
        only_if_enabled = true;
        ansible_prefix = "k8s_cert_manager_";
        inventory_path = "all/cert-manager.yaml";
      }
    )
  ];
}
